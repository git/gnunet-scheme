<TeXmacs|2.1>

<project|scheme-gnunet.tm>

<style|tmmanual>

<\body>
  There are a number of modules for accessing GNUnet
  configurations<index|configuration><label|configuration>.<space|1em>Firstly,
  there is <scm|(gnu gnunet config db)><index|(gnu gnunet config db)>, which
  is the module library code would typically use.<space|1em>For testing, one
  can create an empty configuration with the procedure
  <scm|hash-\<gtr\>configuration><index|hash-\<gtr\>configuration> from that
  module and <scm|make-hashtable> from <scm|(rnrs hashtables)>, using
  <scm|hash-key><index|hash-key> as hash function and
  <scm|key=?><index|key=?> as comparison function:

  <\scm-code>
    (import (gnu gnunet config db)

    \ \ \ \ \ \ \ \ (gnu gnunet config value-parser)

    \ \ \ \ \ \ \ \ (rnrs hashtables))

    (define config (hash-\<gtr\>configuration (make-hashtable hash-key
    key=?))
  </scm-code>

  The resulting configuration <scm|config> is initially empty, so set some
  <with|font-shape|italic|keys> in the <with|font-shape|italic|section>
  <verbatim|nse>, to configure the network-size estimation service:

  <\scm-code>
    (set-value! identity config "nse" "UNIXPATH"
    "/tmp/nse.sock")<index|set-value!>

    (set-value! number-\<gtr\>string config "cadet" "MAX_ROUTES" 5000)

    ;; TODO: IP address, time durations, booleans, ...
  </scm-code>

  Now read these values back:

  <\scm-code>
    (read-value value-\<gtr\>file-name config "nse"
    "UNIXPATH")<index|read-value>

    ;; -\<gtr\> /tmp/nse.sock

    (read-value value-\<gtr\>natural config "cadet" "MAX_ROUTES")

    ;; -\<gtr\> 5000
  </scm-code>

  What if the configuration doesn't have a value for the specified section
  and key?<space|1em>Then an <scm|&undefined-key-error><index|&undefined-key-error>
  results:

  <\scm-code>
    (read-value value-\<gtr\>natural config "kated" "MAX_ROUTES")

    ;; -\<gtr\>

    ;; ice-9/boot-9.scm:1685:16: In procedure raise-exception:

    ;; ERROR:

    ;; \ \ 1. &undefined-key-error:

    ;; \ \ \ \ \ \ section: "kated"

    ;; \ \ \ \ \ \ key: "MAX_ROUTES"
  </scm-code>

  <section|Locating configuration files>

  There are two \U possibly non-existent \U configuration files: the
  <with|font-shape|italic|user> configuration<subindex|configuration|user>
  and the <with|font-shape|italic|system>
  configuration<subindex|configuration|system>.<space|1em>The
  <with|font-shape|italic|system> configuration typically contains the paths
  for services like NSE, CORE, <text-dots><space|1em>A user may choose not to
  use the services by the system and instead use their own.<space|1em>To do
  so, the user needs to override the paths in the
  <with|font-shape|italic|user> configuration.<space|1em><todo|defaults?> The
  module <scm|(gnu gnunet config fs)><index|(gnu gnunet config fs)> is
  responsible for determining the location of the configuration files to load
  and actually load configuration files.<space|1em>For determining the
  location of the configuration files, the procedures
  <scm|locate-user-configuration> and <scm|locate-system-configuration> can
  be used.

  <\warning>
    The C implementation's mechanism for user-system separation seems to work
    differently.
  </warning>

  <\explain>
    <scm|(locate-user-configuration <scm|#:getenv=getenv>)><index|locate-user-configuration>
  <|explain>
    This procedure determines the location of the user configuration file, as
    a string, or <scm|#false> if it could not be determined.<space|1em>If the
    location of the user configuration file is known, but the file does not
    exist, it is returned anyway, as a string.

    If the environment variable <shell|XDG_CONFIG_HOME> is set, the location
    of the file <verbatim|gnunet.conf> in the directory
    <shell|$XDG_CONFIG_HOME> is returned.<space|1em>If the environment
    variable is not set, the location of the file at
    <verbatim|.config/gnunet.conf><index|~/.config/gnunet.conf> in the home
    directory specified by the environment variable <verbatim|HOME> is
    returned, if that environment variable exists.<space|1em>If both are
    unset, <scm|#false> is returned.

    The values of environment variables is determined with the procedure
    <scm|getenv>.
  </explain>

  <\explain>
    <scm|(locate-system-configuration)><index|locate-system-configuration>
  <|explain>
    This procedure determines the location of the system configuration file,
    as a string.

    Currently, this is always <verbatim|/etc/gnunet.conf><index|/etc/gnunet.conf>.
  </explain>

  <section|Loading configuration files>

  Once the location of the configuration file is known, the file can be
  opened with the Scheme procedure <scm|open-input-file>, which returns an
  input port.<space|1em>Then the procedure <scm|load-configuration/port!> can
  be used to determine all section-key-values triples in the configuration.

  <\explain>
    <scm|(load-configuration/port! <var|set-value!>
    <var|port>)><index|load-configuration/port!>
  <|explain>
    Load the configuration from the input port <var|port>.

    For each variable, call <var|set-value!> with the section name, variable
    name and a vector of the form <scm|#(line line-number value)>, where
    <var|value> a list of expansible objects.

    <todo|document expansible objects><todo|error reporting>
  </explain>

  A variable assignment<index|variable assignment> <verbatim|[section]
  key=value${var}> can refer to variables defined in the <verbatim|PATHS>
  section and variables from the environment.<space|1em>The previously
  described procedure <scm|load-configuration/port!> will <em|not> expand
  such assignements. \ To expand variable assignments, use the procedure
  <scm|make-expanded-configuration> instead.

  <\explain>
    <scm|(make-expanded-configuration <var|load!>
    #:getenv=<var|getenv>)><index|make-expanded-configuration>
  <|explain>
    Make a configuration object.<space|1em>To populate the configuration, all
    the procedure <var|load!> with a <scm|set-value!> procedure as expected
    by <scm|load-configuration/port!>.<space|1em>The values from
    <scm|set-value!> are added to the confoiguration and every variable is
    expanded.
  </explain>

  To automatically load the defaults, the system configuration and the user
  configuration, use the thunk <scm|load-configuration>:

  <\explain>
    <scm|(load-configuration #:getenv=<var|getenv>
    #:files=<text-dots>)><index|load-configuration>
  </explain|Load the defaults, the system configuration and the user
  configuration and return the resulting configuration object.<space|1em>The
  list of files to load can be overriden by setting the undocumented
  <var|files> keyword argument.>

  Applications (whether graphical or textual) are recommended to use
  <scm|load-configuration> by default, as it largely just works.
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|save-aux|false>
  </collection>
</initial>