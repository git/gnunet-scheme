<TeXmacs|2.1>

<project|scheme-gnunet.tm>

<style|tmmanual>

<\body>
  Two arbitrary peers on GNUnet can communicate with the CADET<index|CADET>
  service<\footnote>
    With some restrictions: there must be a path between the nodes, in a
    lossy and malicious network with few connections the connection will be
    lossy, etc.
  </footnote>. The client code is available from the <scm|(gnu gnunet cadet
  client)><index|(gnu gnunet cadet client)> module, which implements the
  <reference|server object> pattern \U <scm|connect><subindex|connect|CADET>
  connects to the service and returns a <dfn|CADET server
  object><subindex|server object|CADET> and
  <scm|disconnect!><subindex|disconnect!|CADET> breaks the connection.

  Before describing in more detail the API the software can use, some
  background on CADET's capabilities is given.

  <\warning>
    The API is not yet complete (listening is not yet supported) and some
    aspects (reconnecting, garbage collection) are unimplemented or untested.
    This is expected to improve in the future.
  </warning>

  <section|Qualities and limitations \V avoiding reinventing the wheel>

  <dfn|Channels><index|tunnel><\footnote>
    They could have been called <em|connections>, but that means something
    different in CADET.
  </footnote> \U i.e., bidirectional communication channels between two peers
  over CADET, are encrypted and authentication and hence are impractical to
  eavesdrop on or be modified by an attacker<\footnote>
    Assuming that the cryptography is not broken.
  </footnote>. To aid reliability and performance, CADET aims to keep three
  independent connections between the two peers so manually creating multiple
  channels ought to be unnecessary.

  The unit of information sent over a channel is a
  <with|font-shape|italic|message> \U essentially, a bytevector. The software
  can select multiple options: it can choose between <em|reliable> or
  <em|unreliable>, <em|in-order> and <em|out-of-order> and <em|low-latency>
  or <em|corked>, see <reference|mq-prio-prefs>.

  <todo|ACKs for messages, opening, closing, window size>

  <section|Addresses>

  To contact a peer over CADET, the remote peer must have an <dfn|open
  port><index|port> and the local peer needs to contact this port. The remote
  peer\Uport identifier pair is called a <dfn|CADET address><index|CADET
  address> in Scheme-GNUnet. A <dfn|CADET address> is represented by the
  <acronym|cisw> (<reference|cisw>) type cadet-address:

  <\explain>
    <scm|(make-cadet-address <var|peer> <var|port>)><index|make-cadeet-address>

    <scm|(make-cadet-address/share <var|peer>
    <var|port>)><index|make-cadet-address/share>
  <|explain>
    Make a CADET address for the peer <var|peer> (a readable bytevector slice
    containing a <scm|/peer-identity>) at the port <var|port> (a readable
    bytevector slice containing a <scm|/hashcode:512>), subject to the
    <acronym|cisw> restrictions.
  </explain>

  <\explain>
    <scm|cadet-address-peer><index|cadet-address-peer>,
    <scm|cadet-address-port><index|cadet-address-port>,
    <scm|cadet-address=?><index|cadet-address=?>,
    <scm|cadet-address?><index|cadet-address?>
  </explain|Standard <acronym|cisw> procedures. CADET addresses cannot be
  compared with <scm|equal?>.>

  Guile has a generic interface for network addresses, see (guile)Network
  Socket Address. If BSD socket integration is activated (see <todo|todo>),
  this interface is extended to support CADET addresses. In particular,
  <scm|sockaddr:fam> will return <scm|AF_CADET> (a symbol, also available as
  the variable <scm|AF_CADET>) an <scm|sockaddr:addr> and <scm|sockaddr:port>
  will return the peer and port respectively as a bytevector that should not
  be modified<\footnote>
    A copy is made to avoid letting buggy code mutate the CADET address, but
    this is an implementation detail.
  </footnote>.

  <section|Listening at an address>

  To listen at some address such that other peers can connect, the
  <scm|open-port!> procedure is used.

  <\explain>
    (open-port! <var|server> <var|port> <var|connected> HANDLERS)
  </explain|>

  <section|Connecting to an address>

  <index|open-channel!>To connect to a CADET address, the <scm|open-channel!>
  procedure is used:

  <\explain>
    <scm|(open-channel! <var|server> <var|address> <var|handlers>)>
  <|explain>
    Asynchronuously connect to the CADET address <var|address> via the CADET
    server object <var|server>, returning a CADET channel object. When a
    message is received, it is passed to the appropriate handler.

    <todo|re-entrancy \U running open-channel! inside a #:connected
    fallback?>

    <todo|errors>

    <todo|behaviour in case of blocking>
  </explain>

  To actually send and receive messages, the
  <scm|channel-message-queue><index|channel-message-queue> can be used to
  retrieve the <em|<reference|message queue>> of the channel and the
  procedure <scm|<reference|send-message!>> can be used to send the message
  to the message queue.

  When done with the channel, it should be <dfn|closed>, to save resources.
  This is done with <scm|>the procedure <scm|close-channel!><index|close-channel!>:

  <\explain>
    <scm|(close-channel! <var|channel>)>
  <|explain>
    Close the channel <var|channel>. This is an asynchronuous operation, it
    does not have an immediate effect. This is an idempotent operation,
    closing a channel twice does not have any additional effect.

    Any buffered messages before the call to the first <scm|close-channel!>,
    will still be sent to the service (unless cancelled and until the
    <scm|<scm|disconnect!>>). If messages requested reliable transport, then
    CADET will still retransmit lost messages even though the channel is
    closed or closing.

    For buffered messages not before<footnote|Warning: in a concurrent
    setting, \<#2018\>after' is not the same as \<#2018\>not
    before\<#2019\>.> the call, it is unspecified whether they will still be
    transmitted.
  </explain>

  Alternatively, you can do close the message queue of the channel with
  <scm|(close-queue! (channel-message-queue <var|channel>))>, it's
  equivalent. In fact, that's exactly how <scm|close-channel!> is
  implemented!

  <section|Performing I/O \U GNUnet style>

  <section|Performing I/O \U BSD style>

  <scm|send>, <scm|recv!> (MSG_PEEK), getsockname \U\<gtr\> ???, getpeername
  \U\<gtr\> ???, accept, listen, bind, shutdown

  No reuseport

  <section|BSD socket integration>

  (AF_CADET, SOCK_RAW, 0) \U\<gtr\> GNUnet messages (with reliability
  according to socket options).

  (AF_CADET, SOCK_STREAM, 0) \U\<gtr\> TCP/CADET (VPN)

  (AF_CADET, SOCK_DGRAM, 0) \U\<gtr\> UDP/CADET (VPN)

  (AF_CADET, SOCK_SEQPACKET, 0) \U\<gtr\> UDP/CADET (VPN), in-order, reliable

  (AF_CADET, SOCK_RDM, 0) \U\<gtr\> UDP/CADET (VPN), out-of-order, reliable

  These sockets not dupable, don't have a fd.<todo|>

  <section|Constructing and analysing network messages>

  <todo|difference between channels and ports>

  <todo|acknowledgement>

  <todo|readable bytevector slices>

  <todo|verify against GNUnet documentation>

  <todo|analysis>

  <\explain>
    (construct-open-local-port <var|port>)<index|construct-open-local-port>
  </explain|Create a new <scm|/:msg:cadet:local:port:open><index|/:msg:cadet:local:port:open>
  message for instructing the service to open the port <var|port> at the
  local peer.>

  <\explain>
    (construct-close-local-port <var|port>)<index|construct-close-local-port>
  </explain|Crreate a new <scm|/:msg:cadet:local:port:close><index|/:msg:cadet:local:port:close>
  message for closing the port.>

  <\explain>
    (construct-local-data <var|client-channel-number> <var|priority>
    <var|data>)<index|construct-local-data>
  </explain|Construct a new <scm|/:msg:cadet:local:data><index|/:msg:cadet:local:data>
  message for sending <var|data> \ <todo|client\<less\>-\<gtr\>service>
  <todo|to/from> the channel with identifier <var|client-channel-number> at
  the priority specified by the numeric priority-preference value
  <var|priority>.>

  <\explain>
    <scm|(construct-local-acknowledgement
    <var|client-channel-number>)><index|construct-local-acknowledgement>
  </explain|Construct a new <scm|/:msg:cadet:local:acknowledgement><index|/:msg:cadet:local:acknowledgemeent>
  message for the channel with identifier <var|client-channel-number>, for
  informing the client that the service is ready for more data.>

  <\explain>
    <scm|(construct-local-channel-create <todo|arguments> <var|#:options>)>
  </explain|Construct a new <scm|msg:cadet:local:channel:create> message for
  <todo|which direction? client\<less\>-\<gtr\>service, reasonable options>>

  <\explain>
    (construct-destroy-local-channel <var|client-channel-number>)
  <|explain>
    Construct a new <scm|msg:cadet:local:channel:destroy> message to destroy
    the channel with identifier <var|client-channel-number>. This can be sent
    in two directions: from the client to the service, to close the channel,
    or from service to client, to inform the channel has been closed.

    <todo|Can the service send this without the client requesting this? If
    the client send this message, will the service send this message in
    return as an acknowledgment?>
  </explain>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|save-aux|false>
  </collection>
</initial>