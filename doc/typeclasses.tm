<TeXmacs|2.1>

<project|scheme-gnunet.tm>

<style|tmmanual>

<\body>
  The client APIs for the various GNUnet services have similar APIs. To avoid
  redundantly restating the API for each service type, the common patterns
  are stated here once and are referenced by the service-specific
  documentation.

  <section|Server objects>

  GNUnet has many <dfn|services><index|service>. The module responsible for
  interacting with the service is conventionally named <scm|(gnu gnunet NAME
  client)>, where <var|NAME> depends on the service. To use a service, it
  needs to be <dfn|connected><index|connect> to, with a procedure named
  <scm|connect> and some optional keyword arguments. This procedure can be
  called as <scm|(connect <var|config> #:connected <var|connected>
  #:disconnected <var|disconnected>)>. It returns a <dfn|server
  object><index|server object><label|server object> \U for each service,
  there is a different type of server object.

  The connection is made asynchronuously; the thunk <var|connected> will be
  called when the connection has actually been made.

  When the connection is lost, the (optional) thunk <var|disconnected> is
  called and the implementation will retry connecting. To close the current
  connection, if any, and stop reconnecting, the idempotent procedure
  <scm|disconnect!><index|disconnect!><label|disconnect!> can be called on
  the server object. The server object will also be disconnected after the
  server object becomes unreachable.

  Service types can support or require additional arguments.

  <section|Server-associated objects>

  <\todo>
    todo
  </todo>

  <section|Cooperative immutable slice wrappers (<acronym|cisw>)
  <index|cisw><index|cooperative immutable slice wrapper>><label|cisw>

  Records in Scheme-GNUnet often contain bytevector slices. When constructing
  such a record, it would be inefficient to copy the whole bytevector slice.
  As such, constructors of such record types typically don't do that, then.
  At the same time, it usually is expected that the record is fully
  immutable, and hence that the contents of the slices it contains don't
  change over time. However, sometimes the slices do change over time so a
  copy needs to made even if it is somewhat inefficient.

  Therefore, these record types have two constructors: a constructor
  conventionally named <scm|type-name/share> that does not make a copy and
  requires that the slices are unmodified as long as the constructed object
  remains in use, and a constructor conventionally named <scm|type-name> that
  does make a copy and does not impose such requirements.

  Sometimes, you receive a record that is only valid for a limited duration,
  because afterwards the slices will be modified. To extend the duration, it
  then is required to make a <em|copy> of the record that contains a copy of
  the bytevector slices, and use the copy instead of the original. Procedures
  for making such copies are conventionally named <scm|copy-type-name>, can
  be called as <scm|(copy-type-name <var|original-record>)> and return the
  copy.

  These kind of record types are called <acronym|cisw> in Scheme-GNUnet. You
  can contribute to Scheme-GNUnet by finding a better acronym.

  As the records are immutable, field accessors for slices will always return
  a read-only bytevector slice, even if a read-write bytevector slice was
  passed to the constructor and even if the copying constructor was used.

  Because bytevector slices cannot be compared with <scm|equal?>, the same
  holds for <acronym|cisw> types. Instead, <acronym|cisw> types define their
  own equality procedures, conventionally named <scm|type-name=?>.
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|save-aux|false>
  </collection>
</initial>