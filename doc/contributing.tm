<TeXmacs|2.1>

<project|scheme-gnunet.tm>

<style|generic>

<\body>
  <section|Building from source>

  The latest \<#2018\>official\<#2019\> development version of scheme-GNUnet
  can be found at <hlink|https://git.gnunet.org/gnunet-scheme.git/|https://git.gnunet.org/gnunet-scheme.git/>.
  It can be downloaded with <hlink|git|https://en.wikipedia.org/wiki/Git>.
  The following software needs to be installed first:

  <\itemize>
    <item>The Autotools (<hlink|autoconf|https://www.gnu.org/software/autoconf/>
    and <hlink|automake|https://www.gnu.org/software/automake/>)

    <item><hlink|GNU Guile|https://www.gnu.org/software/guile/> (at least
    version 3), with the patch from <slink|https://debbugs.gnu.org/cgi/bugreport.cgi?bug=49623>
    applied <todo|make optional, choice between Racket and Guile>

    <item>Optionally, Racket, to build the work-in-progress Racket port.

    <\warning>
      Racket is currently unsupported, at it only recognises <scm|\|symbol\|>
      in Racket mode and not in R6RS mode. Once the \<#2018\>r7rs' Racket
      package is packaged in distributions, the porting can continue.
    </warning>

    <item><hlink|Purely Functional Data Structures in Scheme
    (pfds)|https://github.com/ijp/pfds/>

    <item><hlink|(Guile) Fibers|https://github.com/wingo/fibers/>, with the
    patches from <slink|https://github.com/wingo/fibers/pull/50> and
    <slink|https://github.com/wingo/fibers/pull/62/>.

    <item><hlink|Guile-QuickCheck|https://ngyro.com/software/guile-quickcheck.html>

    <item><hlink|Guile-Gcrypt|https://notabug.org/cwebber/guile-gcrypt>
  </itemize>

  A few bug fixes to Guile and Guile Fibers are required that might not yet
  be included in your distribution, see <verbatim|guix.scm>

  Users of <hlink|GNU Guix|https://guix.gnu.org><index|Guix> can run
  <shell|guix shell -D -f guix.scm> in the checkout to create an environment
  where these dependencies are all present.<space|1em>Scheme-GNUnet uses the
  standard GNU build system, so to build Scheme-Gnunet, you only need to run

  <\shell-code>
    autoreconf -vif

    ./configure

    make make-racket # only when using Racket

    make # only when using Guile Scheme

    make check
  </shell-code>

  After building, the documentation is available at
  <verbatim|doc/scheme-gnunet.pdf> and <verbatim|doc/scheme-gnunet.html> in
  PDF and HTML formats.<space|1em>To get started, you can run the example
  mini-application at <verbatim|examples/nse-web.scm> and point your browser
  at <slink|http://localhost:8089>:

  <\shell-code>
    $ guile -L . -C . -l examples/nse-web.scm

    \ \ <todo|TODO racket equivalent>
  </shell-code>

  <subsection|Authenticating new source code><index|authentication>

  When GNU Guix is present, after pulling the latest Scheme-GNUnet commit,
  the following command can be run to verify it is authentic:

  <\shell-code>
    guix git authenticate 088ea8b27b95143584cbc36202c5a02dfe95796c "C1F3 3EE2
    0C52 8FDB 7DD7 \ 011F 49E3 EE22 1917 25EE"
  </shell-code>

  If it isn't authentic, an error message such as the following will be
  written:

  <\shell-code>
    Authenticating commits 54a74dc to 431f336 (1 new commits)...

    [#########################################################]

    guix git: error: commit 431f336edd51e1f0fe059a6f6f2d4c3e9267b7bc not
    signed by an authorized key: C1F3 3EE2 0C52 8FDB 7DD7 \ 011F 49E3 EE22
    1917 25EE
  </shell-code>

  <section|Writing tests>

  <index|tests>\<#2018\><hlink|How SQLite Is
  Tested|https://sqlite.org/testing.html>\<#2019\> is a recommended
  read.<space|1em>Scheme-GNUnet isn't that well-tested but still aims for
  being free of bugs and having many tests to prevents bugs from being
  introduced.<space|1em>When adding new code, consider writing test
  cases.<space|1em>Some things that can be tested and few methods for testing
  things:

  <\itemize>
    <item>Run mutation tests.<space|1em>That is, replace in the source code
    <scm|\<less\>> with <scm|\<less\>=>, <scm|0> with <scm|1>, a variable
    reference <scm|i> with a variable reference <scm|j>, swap destination and
    source arguments <text-dots> and verify whether the tests detect these
    little mutations.

    <item>Be exhaustive.<space|1em>If a procedure handles both foos and bars,
    write test cases that pass the procedure a foo and test cases that pass
    the procedure a bar.<space|1em>Sometimes Guile-QuickCheck can help with
    generating many test cases if the input has a regular structure yet many
    edge cases, see e.g. <verbatim|tests/cmsg.scm>.

    <item>Verify exception mechanisms!<space|1em>If a procedure is expected
    to handle I/O errors, simulate I/O errors and end-of-files in all the
    wrong places.<space|1em>If the procedure can raise exceptions, make sure
    these exceptions are raised when necessary.
  </itemize>

  Tests are added in the directory <scm|tests> and to the variable
  <verbatim|SCM_TESTS> in <verbatim|Makefile.am> and use <scm|srfi
  :64>.<space|1em>To run the test suite, run <verbatim|make check>.

  <section|Writing portable Scheme code>

  Scheme-GNUnet aims to be portable between Schemes. It primarily supports
  Guile Scheme, but also supports Racket Scheme. However, the two different
  Schemes are incompatible. One of these is that Racket expects Scheme files
  to use <verbatim|.ss> as file extension, whereas Guile uses
  <verbatim|.scm>.. Another incompatibility is that Racket expects the Scheme
  files to start with <verbatim|#!r6rs>. To smooth over these two
  differences, <code*|make make-racket> generates <verbatim|.ss> from the
  <verbatim|.scm> files.

  <section|Contact>

  Scheme-GNUnet is currently maintained on NotABug:
  <slink|https://notabug.org/maximed/scheme-gnunet/>.<space|1em>Issues and
  pull requests can be reported and submitted here.<space|1em>Alternatively,
  for discussion about developing Scheme-GNUnet, you can send mails to
  <hlink|gnunet-devel@gnu.org|mailto:gnunet-devel@gnu.org> and for help about
  how to use Scheme-GNUnet, you can contact
  <hlink|help-gnunet@gnu.org|mailto:help-gnunet@gnu.org>.<space|1em>These are
  public mailing lists, so don't send anything there you wouldn't mind the
  whole world to know.

  For security-sensitive issues, you can send a mail directly to the
  maintainer, <hlink|Maxime Devos \<less\>maximedevos@telenet.be\<gtr\>|mailto:maximedevos@telenet.be>,
  optionally encrypted and signed with a GnuPG-compatible
  system.<space|1em>The maintainer's key fingerprint is C1F3 3EE2 0C52 8FDB
  7DD7 011F 49E3 EE22 1917 25EE and a copy of the key can be downloaded from
  <slink|https://notabug.org/maximed/things/raw/master/Maxime_Devos.pub>.

  <section|License>

  The code of Scheme-GNUnet is available under the Affero General Public
  License (AGPL), version 3 or later; see individual source files for
  details.<space|1em>The documentaton is available under the GNU Free
  Documentation License, see the start of this manual and the likewise-named
  appendix for details.<space|1em>The AGPL has some unusual conditions w.r.t.
  applications interacting with the network, please read it carefully.
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|save-aux|false>
  </collection>
</initial>