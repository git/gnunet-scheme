# Copyright (C) 2020, 2021 GNUnet e.V.
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

See doc/scheme-gnunet.tm for documentation.
Or run "make doc/scheme-gnunet.pdf" to read the PDF documentation.
The documentation in this file is in the progress of being moved
to doc/scheme-gnunet.tm.  Scheme-GNUnet is under the AGPL and FDL
(code and documentation), see doc/scheme-gnunet.tm and individual
source files for details.

The following is a left-over, see doc/scheme-gnunet.tm for documentation
instead!

* scheme-GNUnet: a partial Scheme port of GNUnet
  TODO: ask upstream of use of name is acceptable
  TODO: more bindings, less duplication
  TODO: document directory & meta data format
* things that work
  TODO: test suite for download & publish.
  TODO (elsewhere): GNUnet service definitions for Guix in container

** DONE publishing store items
   (Script: gnu/gnunet/scripts/publish-store.scm)
   (Described in ROADMAP.org)

   We cheat by calling the gnunet-publish binary.
   Use this to publish a directory from the store!
** DONE downloading store items
   (Script: gnu/gnunet/scripts/download-store.scm)
   (Described in ROADMAP.org)

   The inverse of the former, to be implemented.
   When implemented, contact guix-devel on how
   to proceed.  Either creates a directory structure
   or a nar.
* Purposes
  + for use by Guix and disarchive
  + bit-for-bit reproducibility in directory creation
  + a nice Scheme interface to GNUnet!
* Modules
  + gnu/gnunet/directory.scm: directory construction
  + gnu/gnunet/concurrency/update.scm: a box with a value,
    that can be updated, resulting in a new box.  Updates
    can be waited upon.
  + gnu/gnunet/utils/platform-enum.scm: Platform-specific
    C-style enum values.
  + gnu/gnunet/concurrency/repeated-condition: different type
    of conditions (TODO describe better)
** Tags
   + spec: it is unknown if this will turn out to be a practical abstraction.
   + why: it remains to be seen if these modules will have any use
   + test: these modules have (passing) tests
   + good: these modules, abstractions ... are practical, and will not be scrapped
     (tweaks might still be possible, and the modules could still have missing
     functionality)
   + wart: these modules have some ‘unniceties’ (warts).  This does /not/ prevent
     the ‘good’ tag.

** More refined IP, TCP, UDP, ...                             :test:good:why:
   + gnu/gnunet/icmp/struct.scm: ICMP packet types & codes
     (incomplete, to be used for error messages)
   + gnu/gnunet/util/cmsg.scm: Constructing & analysing
     ancillary messages (likewise)

   TODO: IP_PKTINFO for interface address, scope ...
   TODO: message queue based upon this
   TODO: nice abstraction for network errors
** Relative time manipulation                                     :test:good:
   + gnu/gnunet/time.scm: Time units and exponential back-off.
