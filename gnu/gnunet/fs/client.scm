;#!r6rs
;; This file is part of Scheme-GNUnet
;; Copyright © 2022 GNUnet e.V.
;;
;; Scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; Scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Author: Maxime Devos

;; TODO: maybe rename to (gnu gnunet file-sharing client),
;; and likewise for the other services?
(define-library (gnu gnunet fs client)
  (export (rename (server:fs? server?))
	  connect disconnect!)
  (import (only (rnrs base) begin lambda define apply quote)
	  (only (rnrs records syntactic) define-record-type)
	  (only (guile) define*)
	  (only (ice-9 match) match)
	  (only (gnu gnunet mq handler) message-handlers)
	  (only (gnu gnunet server) <server> make-disconnect!
		spawn-server-loop run-loop loop:terminal-condition
		handle-control-message!)
	  (only (srfi srfi-26) cut))
  (begin
    (define-record-type (<server:fs> make-server:fs server:fs?)
      (parent <server>)
      (protocol (lambda (%make) (lambda () ((%make))))))

    (define (make-message-handlers . _)
      (message-handlers)) ; TODO

    (define (control-message-handler message continue continue* message-queue
				     loop)
      (define (k/reconnect!)
	(run-loop loop))
      (match message
        (('resend-old-operations!) ; TODO no operations implemented yet
	 (continue loop))
        (('lost . _)
	 ;; Server became unreachable, disconnect.  Tested by the
	 ;; "garbage collectable" test.
	 (continue* '(disconnect!) loop))
        (rest (handle-control-message! message message-queue
				       (loop:terminal-condition loop)
				       (cut run-loop loop)))))

    (define* (connect config #:key connected disconnected spawn #:rest r)
      "Connect to the file-sharing service in the background.  This is an
instance of the @code{connect} procedure of the @emph{server object} pattern."
      (apply spawn-server-loop (make-server:fs)
	     #:make-message-handlers make-message-handlers
	     #:control-message-handler control-message-handler
	     #:configuration config
	     #:service-name "fs" r))

    (define disconnect! (make-disconnect! 'fs server:fs?))))
