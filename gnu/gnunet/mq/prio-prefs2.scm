;#!r6rs
;; This file is part of scheme-GNUnet.
;; Copyright (C) 2012-2016, 2021 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; @brief General-purpose message queue (priorities, documentation)

;; Work around self-references.
;; (enum-value ENUM sym) cannot be used in the
;; same module where ENUM is defined, as
;; when expanding enum-value, the module where
;; ENUM is loaded first.
(define-library (gnu gnunet mq prio-prefs2)
  (export priority-preference priority-preference?)
  (import (gnu extractor enum)
	  (only (rnrs base) begin))
  (begin
    (define-enumeration (priority-preference priority-preference?)
      (#:documentation
       "Per envelope preferences and priorities.
This is to be used as a bit set, e.g. using @code{prio-pref->integer}
and @code{integer->prio-pref}.  Priority-preferences are usually
passed as integers to avoid conversion hence and forth.")
      (#:max 8)
      (#:known
       (value
	(documentation "Best-effort traffic (i.e. CADET relay, DHT)")
	(symbol prio:best-effort)
	(index 0))
       (value
	(documentation "Urgent traffic (local peer, i.e. Conversation).")
	(symbol prio:urgent)
	(index 1))
       (value (symbol pref:0)
	      (documentation "[Unassigned]")
	      (index 2))
       (value (symbol pref:1)
	      (documentation "[Unassigned]")
	      (index 3))
       (value
	(symbol pref:unreliable)
	(documentation
	 "Flag to indicate that unreliable delivery is acceptable.  This
means TRANSPORT will not attempt to receive an
acknowledgment. CORE will just pass this flag through.  CADET
will use unreliable delivery if this flag is set.

Note that even without this flag, messages may be lost by
TRANSPORT and CORE.

Thus, how \"strong\" the semantics of reliable delivery are depends
on the layer!")
	(index 4))
       ;; FIXME spelling error in src/include/gnunet_mq_lib.h
       (value
	(symbol pref:low-latency)
	(documentation
	 "Flag to indicate that low latency is important.  This flag must
generally not be used in combination with
pref:cork-allowed as it would be a contradiction.
When this flags is set, the envelope may skip forward in the
queue (depending on priority) and also TRANSPORT should attempt
to pick a communicator with particularly low latency.")
	(index 5))
       (value
	(symbol pref:cork-allowed)
	(documentation
	 "Flag to indicate that CORKing is acceptable. This allows the
receiver to delay transmission in hope of combining this message
with other messages into a larger transmission with less
per-message overhead.")
	(index 6))
       (value
	(symbol pref:good-throughput)
	(documentation
	 "Flag to indicate that high bandwidth is desired. This flag
indicates that the method chosen for transmission should focus on
overall goodput.  It rarely makes sense to combine this flag with
pref:low-latency")
	(index 7))
       (value
	(symbol pref:out-of-order)
	(documentation
	 "Flag to indicate that out-of-order delivery is OK.")
	(index 8))))))
