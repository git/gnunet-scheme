;#!r6rs
;;   This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;;   Copyright (C) 2006--2021 GNUnet e.V.
;;
;;   GNUnet is free software: you can redistribute it and/or modify it
;;   under the terms of the GNU Affero General Public License as published
;;   by the Free Software Foundation, either version 3 of the License,
;;   or (at your option) any later version.
;;
;;   GNUnet is distributed in the hope that it will be useful, but
;;   WITHOUT ANY WARRANTY; without even the implied warranty of
;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;   Affero General Public License for more details.
;;
;;   You should have received a copy of the GNU Affero General Public License
;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;   SPDX-License-Identifier: AGPL-3.0-or-later

;; Extracted from src/include/gnunet_common.h
(define-library (gnu gnunet hashcode struct)
  (export /hashcode:512 /hashcode:256)
  (import (only (rnrs base) begin quote)
	  (only (gnu gnunet netstruct syntactic)
		define-type structure/packed)
	  (only (gnu gnunet netstruct procedural) u8vector))
  (begin
    (define-type /hashcode:512
      (structure/packed
       (synopsis "A 512-bit hashcode.")
       (documentation
	"These are the default length for GNUnet, using SHA-512.")
       (properties '((c-type . GNUNET_HashCode)))
       (field (bits/u8 (u8vector 64)))))

    (define-type /hashcode:256
      (structure/packed
       (synopsis "A 256-bit hashcode.")
       (documentation
	"Used under special conditions, like when space
is critical and security is not impacted by it.")
       (properties '((c-type . GNUNET_ShortHashCode)))
       (field (bits/u8 (u8vector 32)))))))
