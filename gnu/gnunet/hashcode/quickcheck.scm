;#!r6rs
;;   This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;;   Copyright © 2022 GNUnet e.V.
;;
;;   scheme-GNUnet is free software: you can redistribute it and/or modify it
;;   under the terms of the GNU Affero General Public License as published
;;   by the Free Software Foundation, either version 3 of the License,
;;   or (at your option) any later version.
;;
;;   scheme-GNUnet is distributed in the hope that it will be useful, but
;;   WITHOUT ANY WARRANTY; without even the implied warranty of
;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;   Affero General Public License for more details.
;;
;;   You should have received a copy of the GNU Affero General Public License
;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;   SPDX-License-Identifier: AGPL-3.0-or-later
(define-library (gnu gnunet hashcode quickcheck)
  (import (only (rnrs base) begin define)
	  (only (quickcheck arbitrary)
		arbitrary $record)
	  (only (gnu gnunet hashcode)
		make-hashcode:512/share make-hashcode:256/share
		hashcode:512->slice hashcode:256->slice
		hashcode:512-u8-length hashcode:256-u8-length)
	  (only (gnu gnunet utils bv-slice-quickcheck)
		$sized-bytevector-slice/read-only))
  (export $hashcode:512 $hashcode:256)
  (begin
    ;; TODO: give '000...' more weight
    ;; (is it used as a special value somewhere?).
    ;;
    ;; The unshared variant would work too, but is less efficient.
    (define $hashcode:512
      ($record make-hashcode:512/share
	       (hashcode:512->slice
		($sized-bytevector-slice/read-only hashcode:512-u8-length))))
    (define $hashcode:256
      ($record make-hashcode:256/share
	       (hashcode:256->slice
		($sized-bytevector-slice/read-only hashcode:256-u8-length))))))
