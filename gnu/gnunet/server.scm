;#!r6rs
;; This file is part of Scheme-GNUnet
;; Copyright © 2021, 2022 GNUnet e.V.
;;
;; Scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; Scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; TODO: document
(define-library (gnu gnunet server)
  (export maybe-send-control-message!* maybe-send-control-message!
	  maybe-ask* answer
	  make-error-handler make-error-handler*/loop
	  <server> server-terminal-condition server-control-channel
	  make-disconnect!
	  handle-control-message!
	  <loop> make-loop
	  loop:connected loop:disconnected loop:terminal-condition
	  loop:control-channel loop:configuration loop:service-name
	  loop:spawner loop:lost-and-found run-loop spawn-server-loop)
  (import (only (rnrs base)
		begin define cons case else apply values quote lambda
		if error list let and append assert string? procedure?
		list? symbol?)
	  (only (rnrs records syntactic)
		define-record-type)
	  (only (fibers)
		spawn-fiber)
	  (only (fibers conditions)
		make-condition wait-operation signal-condition! condition?)
	  (only (fibers channels)
		make-channel put-operation get-operation put-message
		get-message channel?)
	  (only (gnu gnunet config db)
		configuration?)
	  (only (fibers operations)
		choice-operation perform-operation wrap-operation)
	  (only (gnu gnunet concurrency lost-and-found)
		make-lost-and-found collect-lost-and-found-operation
		losable-lost-and-found lost-and-found?)
	  (only (gnu gnunet mq)
		message-queue? close-queue!)
	  (only (gnu gnunet mq error-reporting)
		report-error)
          (only (gnu gnunet mq-impl stream)
		connect/fibers)
	  (only (ice-9 match)
		match)
	  (only (guile)
	        lambda* define*))
  (begin
    ;; Define them here to avoid creating these objects multiple times.
    (define thunk-false (lambda () #false))
    (define thunk-true (lambda () #true))

    (define (maybe-send-control-message!* terminal-condition control-channel
					  . message)
      "Send @var{message} to the main loop, unless it is stopping or has stopped.

This sends a @var{message} to @var{control-channel} or waits for
@var{terminal-condition} to be signalled, whichever happens first.
If the message is sent, @code{#true} is returned.  Otherwise, if
@var{terminal-condition} was signalled, return @code{#false} instead."
      (assert (condition? terminal-condition))
      (assert (channel? control-channel))
      (perform-operation
       (choice-operation
	;; Nothing to do when the <server> is permanently disconnected,
	;; or is being disconnected.
	(wrap-operation
	 (wait-operation terminal-condition)
	 thunk-false)
	(wrap-operation
	 (put-operation control-channel message)
	 thunk-true))))

    (define (maybe-send-control-message! server . message)
      "Send @var{message} to the control channel of @var{server}, or don't
do anything if @var{server} has been permanently disconnected.  The return
values are the same as for @code{maybe-send-control-message!*}."
      (apply maybe-send-control-message!* (server-terminal-condition server)
	     (server-control-channel server) message))

    (define (maybe-ask* terminal-condition control-channel question . rest)
      "Maybe-send a list @code{(question answer-box . rest)} to the control channel of
the server.  The control channel should put an answer in the answer box with
@code{answer}, when done so, the response value is returned.  In case of a permanent
disconnect, @code{#false} is returned.

The type of @var{answer-box} is an implementation detail."
      (let ((response-channel (make-channel)))
	(and (apply maybe-send-control-message!* terminal-condition
		    control-channel question response-channel rest)
	     (get-message response-channel))))

    (define (answer answer-box answer)
      "The counterpart of @code{maybe-ask*}."
      (put-message answer-box answer)
      (values)) ; for backtraces, if the type of @var{answer-box} is incorrect

    (define (make-error-handler connected disconnected terminal-condition control-channel)
      (define (error-handler key . arguments)
	(case key
	  ((connection:connected)
	   ;; Tell the event loop to resume old requests.
	   (connected)
	   (maybe-send-control-message!* terminal-condition control-channel
					 'resend-old-operations!)
	   (values))
	  ((input:regular-end-of-file input:premature-end-of-file)
	   (disconnected)
	   ;; Tell the event loop that it is time to restart,
	   ;; unless it already stopping.
	   (maybe-send-control-message!* terminal-condition control-channel 'reconnect!))
	  ;; The event loop closed the queue and will exit, nothing to do here!
	  ;;
	  ;; Tested by "(DHT) close, not connected --> all fibers stop,
	  ;; no callbacks called" in tests/distributed-hash-table.scm.
	  ((connection:interrupted)
	   (values))
	  (else
	   ;; Some unknown problem, let the event loop report the error,
	   ;; disconnect and stop reconnecting.  The first two happen
	   ;; in no particular order.
	   (apply maybe-send-control-message!* terminal-condition
		  control-channel 'oops! key arguments)
	   (values))))
      error-handler)

    (define (make-error-handler*/loop loop . _)
      (make-error-handler
       (loop:connected loop) (loop:disconnected loop)
       (loop:terminal-condition loop) (loop:control-channel loop)))

    (define-record-type (<server> %make-server server?)
      (parent <losable>) ; for automatic fibers disposal when the <server> is unreachable
      ;; terminal-condition: a disconnect has been requested.
      (fields (immutable terminal-condition server-terminal-condition)
	      (immutable control-channel server-control-channel))
      (protocol (lambda (%make)
		  (lambda ()
		    ((%make (make-lost-and-found))
		     (make-condition)
		     (make-channel))))))

    (define (make-disconnect! name type?)
      ;; for backtrace purposes, 'lambda' is not used here.
      (define (disconnect! server)
	"Asynchronuously disconnect from the service and stop reconnecting,
even if not connected.  This is an idempotent operation.  This is an
asynchronuous request; it won't be fulfilled immediately.  More technically,
this maybe-sends @code{disconnect!} to the control channel."
	(if (type? server)
	    (maybe-send-control-message! server 'disconnect!)
	    (error 'disconnect! ; TODO: test
		   "wrong server object type"
		   (list name type? server))))
      (assert (symbol? name)) ; XXX not sure which
      (assert (procedure? type?))
      disconnect!)

    
    ;; Originally, lots of keyword arguments were passed, but having a single
    ;; structure with all the persistent state is more convenient.
    (define-record-type (<loop> make-loop loop?)
      (fields (immutable service-name loop:service-name)
	      (immutable control-message-handler loop:control-message-handler)
	      (immutable make-message-handlers loop:message-handlers-maker)
	      (immutable make-error-handler loop:error-handler*-maker)
	      (immutable configuration loop:configuration)
	      (immutable spawn loop:spawner) ; like spawn-fiber
	      ;; string (e.g. "dht", "cadet", ...)
	      (immutable terminal-condition loop:terminal-condition) ; condition
	      (immutable control-channel loop:control-channel) ; <channel>
	      (immutable lost-and-found loop:lost-and-found)
	      (immutable connected loop:connected)
	      (immutable disconnected loop:disconnected))
      (protocol
       (lambda (%make)
	 (lambda* (#:key service-name control-message-handler
		   make-message-handlers
		   (make-error-handler* make-error-handler*/loop)
		   configuration (spawn spawn-fiber) terminal-condition
		   control-channel lost-and-found (connected values)
		   (disconnected values)
		   #:allow-other-keys)
	  (assert (string? service-name))
	  (assert (procedure? control-message-handler))
	  (assert (procedure? make-message-handlers))
	  (assert (configuration? configuration))
	  (assert (procedure? spawn))
	  (assert (condition? terminal-condition))
	  (assert (channel? control-channel))
	  (assert (lost-and-found? lost-and-found))
	  (assert (procedure? connected))
	  (assert (procedure? disconnected))
	  (%make service-name control-message-handler
		 make-message-handlers make-error-handler* configuration
		 spawn terminal-condition control-channel
		 lost-and-found connected disconnected)))))

    (define (handle-control-message! message mq terminal-condition k/reconnect!)
      "The following messages are handled:

@itemize
@item oops!, by signalling @var{terminal-condition}, reporting the error and closing the queue
(not necessarily in that order).
@item disconnect!, by signalling @var{terminal-condition} and closing the queue
@item reconnect!, by calling the thunk @var{k/reconnect} in tail position

TODO: maybe 'lost'"
      (assert (message-queue? mq))
      (assert (condition? terminal-condition))
      (assert (procedure? k/reconnect!))
      (match message
        (('oops! key . arguments)
	 ;; Some unknown error, report it (report-error) and close
	 ;; the queue (close-queue!).  'connected' will be called
	 ;; from the 'input:regular-end-of-file' case in 'error-handler'.
	 ;;
	 ;; The error reporting and closing happen in no particular order.
	 (signal-condition! terminal-condition)
	 (apply report-error key arguments)
	 (close-queue! mq)
	 (values))
	(('disconnect!)
	 ;; Ignore future requests instead of blocking.
	 (signal-condition! terminal-condition)
	 ;; Close networking ports.
	 (close-queue! mq)
	 ;; And the fibers of the <server> object are now done!
	 (values))
	(('reconnect!)
	 ;; Restart the loop with a new message queue.
	 (k/reconnect!))))

    ;; TODO: document, check types
    ;; state: <loop>
    (define (run-loop state . rest)
      (assert (loop? state))
      (define handlers (apply (loop:message-handlers-maker state) state rest))
      (define error-handler
	(apply (loop:error-handler*-maker state) state rest))
      (define message-queue
	(connect/fibers (loop:configuration state) (loop:service-name state)
			handlers error-handler #:spawn (loop:spawner state)))
      ;; As undocumented in (gnu gnunet lost-and-found), lost-and-found
      ;; operations may not be reused (at least, not in all cases).
      (define (make-loop-operation)
	(choice-operation
	 (get-operation (loop:control-channel state))
	 (wrap-operation
	  (collect-lost-and-found-operation (loop:lost-and-found state))
	  (lambda (lost) (cons 'lost lost)))))
      (define (continue* message state . rest)
	;; Let @var{control-message-handler} handle the message.
	;; It can decide to continue with @var{continue} or @var{continue*},
	;; in continuation-passing style.
	(apply (loop:control-message-handler state) message continue continue*
	       message-queue state rest))
      (define (continue state . rest)
	"The main event loop."
	(apply continue* (perform-operation (make-loop-operation)) state rest))
      (apply continue state rest))

    (define* (spawn-server-loop server #:key (make-loop make-loop)
				(initial-extra-loop-arguments '())
				(spawn spawn-fiber) #:allow-other-keys #:rest r)
      (assert (server? server))
      (assert (procedure? make-loop))
      (assert (list? initial-extra-loop-arguments))
      (assert (procedure? spawn))
      "[TODO] and return @var{server}"
      (define loop-arguments
	(append (list #:terminal-condition (server-terminal-condition server)
		      #:control-channel (server-control-channel server)
		      #:lost-and-found (losable-lost-and-found server))
		r))
      (spawn (lambda ()
	       (apply run-loop (apply make-loop loop-arguments)
		      initial-extra-loop-arguments)))
      server)))
