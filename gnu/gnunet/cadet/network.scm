;#!r6rs
;; This file is part of Scheme-GNUnet.
;; Copyright © 2022 GNUnet e.V.
;;
;; Scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; Scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(define-library (gnu gnunet cadet network)
  (export construct-local-channel-create analyse-local-channel-create
	  construct-local-channel-destroy analyse-local-channel-destroy
	  construct-local-data analyse-local-data
	  construct-local-acknowledgement analyse-local-acknowledgement)
  (import (rename (gnu gnunet cadet client)
		  (| construct-local-channel-create|
		   construct-local-channel-create)
		  (| analyse-local-channel-create|
		   analyse-local-channel-create)
		  (| construct-local-channel-destroy|
		   construct-local-channel-destroy)
		  (| analyse-local-channel-destroy|
		   analyse-local-channel-destroy)
		  (| construct-local-data| construct-local-data)
		  (| analyse-local-data| analyse-local-data)
		  (| construct-local-acknowledgement|
		   construct-local-acknowledgement)
		  (| analyse-local-acknowledgement|
		   analyse-local-acknowledgement))))
