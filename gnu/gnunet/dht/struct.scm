;#!r6rs
;; This file is part of GNUnet.
;; Copyright (C) 2001-2013, 2022 GNUnet e.V.
;;
;; GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

(define-library (gnu gnunet dht struct)
  (export %DHT_BLOOM_SIZE
	  /dht:path-element
	  /:msg:dht:client:get:stop
	  /:msg:dht:client:get
	  /:msg:dht:client:get-result-known
	  /:msg:dht:client:result
	  /:msg:dht:client:put
	  /:msg:dht:monitor:put
	  /:msg:dht:monitor:start
	  /:msg:dht:monitor:stop
	  /:msg:dht:monitor:get
	  /:msg:dht:monitor:get-response)
  (import (only (rnrs base)
		define begin quote)
	  (only (gnu gnunet util struct)
		/:message-header /time-absolute)
	  (only (gnu gnunet hashcode struct)
		/hashcode:512)
	  (only (gnu gnunet crypto struct)
		/peer-identity /eddsa-signature)
	  (only (gnu gnunet netstruct syntactic)
		define-type structure/packed)
	  (only (gnu gnunet netstruct procedural)
		u32/big u64/big s16/big))
  (begin
    ;; Size of the bloom filter the DT uses to filter peers.
    (define %DHT_BLOOM_SIZE 128)

    ;; GNUNET_DHT_PathElement in C GNUnet
    (define-type /dht:path-element
      (structure/packed
       ;; TODO: path vs. path element?
       (synopsis "A (signed) path tracking a block's flow through the DHT is
represented by an array of path elements, each consisting of a peer on the path
and a signature by which the peer affirms its routing decision.")
       (properties '((c-type . GNUNET_DHT_PathElement)))
       (field (previous /peer-identity)
	      (synopsis "The previous peer on the path.

The public key used to create the signature is in the
@emph{next} path element, or the sender of the message if this was the last
path element.")) ;; C GNUnet says somethin about a non-existent ‘succ’ field?
       (field (signature /eddsa-signature)
	      (synopsis "Signature affirming the hop, of type
@code{GNUNET_SIGNATURE_PURPOSE_DHT_HOP}."))))

    (define-type /:msg:dht:client:get:stop
      (structure/packed
       (synopsis "Message sent from the client to the DHT service to cancel
an outstanding get request, allowing freeing resources.")
       (properties '((message-symbol msg:dht:client:get:stop)
		     (c-type . GNUNET_DHT_ClientGetStopMessage)))
       (field (header /:message-header))
       (field (reserved u32/big)
	      (synopsis "Currently always zero"))
       (field (unique-id u64/big)
	      (synopsis "Unique ID identifying the get request"))
       (field (key /hashcode:512)
	      (synopsis "Key of the get request"))))

    ;; Possibly followed by xquery.
    (define-type /:msg:dht:client:get
      (structure/packed
       (synopsis "DHT GET message sent from clients to service, indicating a GET
request should be issued.")
       (properties '((message-symbol msg:dht:client:get)
		     (c-type . GNUNET_DHT_ClientGetMessage)))
       (field (header /:message-header)
	      (synopsis "Type: msg:dht:client:get"))
       (field (options u32/big)
	      (synopsis "Message options")) ; TODO enum
       (field (desired-replication-level u32/big)
	      (synopsis "Replication level for this message"))
       (field (type u32/big)
	      (synopsis "The type for the data for the GET request")) ; TODO enum
       (field (key /hashcode:512)
	      (synopsis "The key to search for"))
       (field (unique-id u64/big)
	      (synopsis "Unique ID identifying this request, if 0 then the client
will not expect a response"))))

    ;; Followed by an array of the hash codes of known results.
    (define-type /:msg:dht:client:get-result-known ;; XXX plural?
      (structure/packed
       (synopsis "Message sent from clients to service, indicating a GET request
should exclude certain results which are already known.")
       (properties '((message-symbol msg:dht:client:get-result-known)
		     (c-type . GNUNET_DHT_ClientGetResultSeenMessage)))
       (field (header /:message-header)
	      (synopsis "Type: msg:dht:client:get-results-known"))
       (field (reserved u32/big)
	      (synopsis "Reserved, always 0"))
       (field (key /hashcode:512)
	      (synopsis "The key we are searching for (to make it easy to find the
corresponding GET inside the service)."))
       (field (unique-id u64/big)
	      (synopsis "Unique ID identifying this request"))))

    ;; put path, get path and actual data are copied to the end of this structure
    (define-type /:msg:dht:client:result
      (structure/packed
       (synopsis "Reply to a GET sent from the service to a client")
       (properties '((message-symbol msg:dht:client:result)
		     (c-type . GNUNET_DHT_ClientResultMessage)))
       (field (header /:message-header)
	      (synopsis "Type: msg:dht:client:result"))
       (field (type u32/big)
	      (synopsis "The type for the data"))
       (field (put-path-length u32/big)
	      (synopsis "Number of peers recorded in the outgoing path from
source to the storage location of this message"))
       (field (get-path-length u32/big)
	      (synopsis "The number of peer identities recorded from the storage
location to this peer."))
       (field (unique-id u64/big)
	      (synopsis "Unique ID of the matching GET request"))
       (field (expiration /time-absolute)
	      (synopsis "Expiration date of this entry"))
       (field (key /hashcode:512)
	      (synopsis "The key that was searched for"))))

    ;; Data is copied to the end of the message
    (define-type /:msg:dht:client:put
      (structure/packed
       (synopsis "Message to insert data into the DHT, sent from clients to DHT
service")
       (properties '((message-symbol msg:dht:client:put)
		     (c-type . GNUNET_DHT_ClientPutMessage)))
       (field (header /:message-header)
	      (synopsis "Type msg:dht:client:put"))
       (field (type u32/big)
	      (synopsis "The type of data to insert"))
       (field (option u32/big)
	      (synopsis "Message options")) ; TODO enum
       (field (desired-replication-level u32/big)
	      (synopsis "Replication level for this message"))
       (field (expiration /time-absolute)
	      (synopsis "Requested expiration data of this data"))
       (field (key /hashcode:512)
	      (synopsis "The key to store the value under"))))

    ;; Followed by put path (if tracked) and payload
    (define-type /:msg:dht:monitor:put
      (structure/packed
       (synopsis "Message to monitor put requests going through peer
(DHT service -> clients)")
       (properties '((message-symbol msg:dht:monitor:put)
		     (c-type . GNUNET_DHT_MonitorPutMessage)))
       (field (header /:message-header)
	      (synopsis "Type: msg:dht:monitor:put"))
       (field (options u32/big)
	      (synopsis "Message options")) ; TODO enum
       (field (type u32/big)
	      (synopsis "The type of data in the request"))
       (field (hop-count u32/big)
	      (synopsis "Hop count so far"))
       (field (desired-replication-level u32/big)
	      (synopsis "Replication level for this message"))
       (field (put-path-length u32/big)
	      (synopsis "Number of peers recorded in the outgoing path from
source to the target location of this message."))
       (field (expiration-time /time-absolute)
	      (synopsis "How long the data should persist"))
       (field (key /hashcode:512)
	      (synopsis "The key to store the value under"))))

    (define-type /:msg:dht:monitor:start/stop
      (structure/packed
       ;; TODO: also to stop monitoring messages?
       (synopsis "Message to request monitoring messages, client -> DHT service")
       (properties '((message-symbol msg:dht:monitor:start
				     msg:dht:monitor:stop)
		     (c-type . GNUNET_DHT_MonitorStartStopMessage)))
       (field (header /:message-header)
	      (synopsis "Type: msg:dht:monitor:start or msg:dht:monitor:stop"))
       (field (type u32/big)
	      ;; XXX block types
	      (synopsis "The type of data desired, GNUNET_BLOCK_TYPE_ANY for all"))
       ;; FIXME: grammar
       (field (get? s16/big)
	      (synopsis "Flag whether to notify about GET messages"))
       (field (get-response? s16/big)
	      (synopsis "Flag whether to notify about GET_RESPONSE messages"))
       (field (put? s16/big)
	      (synopsis "Flag whether to notify about PUT messages"))
       (field (filter-key? s16/big)
	      (synopsis "Flag whether to use the provided key to filter messages"))
       (field (key /hashcode:512)
	      (synopsis "The key to filter messages by"))))

    (define-type /:msg:dht:monitor:start /:msg:dht:monitor:start/stop)
    (define-type /:msg:dht:monitor:stop /:msg:dht:monitor:start/stop)

    ;; Followed by get path (if tracked)
    (define-type /:msg:dht:monitor:get
      (structure/packed
       (synopsis "Message to monitor get requests going through peer,
DHT service -> clients.")
       (properties '((message-symbol msg:dht:monitor:get)
		     (c-type . GNUNET_DHT_MonitorGetMessage)))
       (field (header /:message-header)
	      (synopsis "Type: msg:dht:monitor:get"))
       (field (options u32/big)
	      (synopsis "Message options")) ; TODO enum
       (field (type u32/big)
	      (synopsis "The type of data in the request"))
       (field (hop-count u32/big)
	      (synopsis "Hop count"))
       (field (desired-replication-level u32/big)
	      (synopsis "Replication level for this message"))
       (field (get-path-length u32/big)
	      (synopsis "Number of peers recorded in the outgoing path from
source to the storage location of this message"))
       (field (key /hashcode:512)
	      (synopsis "The key to store the value under"))))

    ;; followed by put path (if tracked), get path (if tracked) and payload
    (define-type /:msg:dht:monitor:get-response
      (structure/packed
       (synopsis "Message to monitor get requests going through peer,
DHT service -> clients")
       (properties '((message-symbol msg:dht:p2p:result)
		     (c-type . GNUNET_DHT_MonitorGetRespMessage)))
       (field (header /:message-header)
	      ;; XXX correct?
	      (synopsis "Type: msg:dht:p2p:result"))
       (field (type u32/big)
	      (synopsis "Content type"))
       (field (put-path-length u32/big)
	      (synopsis "Length of the PUT path that follows (if tracked)"))
       (field (get-path-length u32/big)
	      (synopsis "Length of the GET path that follows (if tracked)"))
       (field (expiration-time /time-absolute)
	      (synopsis "When does the content expire?"))))))
