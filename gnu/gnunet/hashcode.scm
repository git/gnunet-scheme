;#!r6rs
;;   This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;;   Copyright (C) 2006--2020, 2022--2023 GNUnet e.V.
;;
;;   GNUnet is free software: you can redistribute it and/or modify it
;;   under the terms of the GNU Affero General Public License as published
;;   by the Free Software Foundation, either version 3 of the License,
;;   or (at your option) any later version.
;;
;;   GNUnet is distributed in the hope that it will be useful, but
;;   WITHOUT ANY WARRANTY; without even the implied warranty of
;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;   Affero General Public License for more details.
;;
;;   You should have received a copy of the GNU Affero General Public License
;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;   SPDX-License-Identifier: AGPL-3.0-or-later

;; Extracted from src/include/gnunet_common.h

(library (gnu gnunet hashcode)
  (export hashcode:512-bit-length hashcode:512-u8-length
          hashcode:256-bit-length hashcode:256-u8-length
          hashcode:512? hashcode:256?
	  hashcode:512=? hashcode:256=?
          make-hashcode:512/share make-hashcode:512
	  make-hashcode:256/share make-hashcode:256
          hashcode:512->slice hashcode:256->slice
	  copy-hashcode:512 copy-hashcode:256)
  (import (rnrs base)
	  (gnu gnunet utils bv-slice)
	  (only (gnu gnunet hashcode struct)
		/hashcode:512 /hashcode:256)
	  (only (gnu gnunet utils records)
		define-record-type*))

  (define hashcode:512-bit-length 512)
  (define hashcode:256-bit-length 256)
  (define hashcode:512-u8-length (/ hashcode:512-bit-length 8))
  (define hashcode:256-u8-length (/ hashcode:256-bit-length 8))

  ;; A 512-bit hashcode.  These are the default length for GNUnet,
  ;; using SHA-512.
  (define-record-type* (<hashcode:512> hashcode:512?)
    #:equality hashcode:512=?
    #:network-structure /hashcode:512
    #:read-only-slice-wrapper #true
    #:unwrap hashcode:512->slice
    #:constructor (make-hashcode:512/share
		   "Make a hashcode, containing @var{slice} (a readable
@code{/hashcode:512} bytevector slice).  @var{slice} may not be mutated
while the constructed hashcode is in use.")
    #:constructor/copy make-hashcode:512
    #:copy (copy-hashcode:512
	    "Make a copy of the hashcode:512 @var{hashcode:512}.  This can be useful if
the slice used during the construction of @var{hashcode:512} is potentially
going to be mutated while a hashcode will still be in use."))

  ;; A 256-bit hashcode.  Used under special conditions, like when space
  ;; is critical and security is not impacted by it.
  (define-record-type* (<hashcode:256> hashcode:256?)
    #:equality hashcode:256=?
    #:network-structure /hashcode:256
    #:read-only-slice-wrapper #true
    #:unwrap hashcode:256->slice
    #:constructor (make-hashcode:256/share
		   "Make a short hashcode, containing @var{slice} (a readable
@code{/hashcode:256} bytevector slice).  @var{slice} may not be mutated
while the constructed short hashcode is in use.")
    #:constructor/copy make-hashcode:256
    #:copy (copy-hashcode:256
	    "Make a copy of the hashcode:256 @var{hashcode:256}.  This can be useful if
the slice used during the construction of @var{hashcode:256} is potentially
going to be mutated while a hashcode will still be in use."))

  (define (bv->hashcode:512 bv)
    "Read a hashcode from a bytevector (deprecated)."
    (make-hashcode:512 (bv-slice/read-only bv)))
  (define (bv->hashcode:256 bv)
    "Read a short hashcode from a bytevector (deprecated)."
    (make-hashcode:256 (bv-slice/read-only bv)))

  (define (hashcode:512->bv hashcode)
    "Extract the bytevector corresponding to @var{hashcode}
(read-only)"
    (slice-copy/bytevector (hashcode:512->slice hashcode)))

  (define (hashcode:256->bv hashcode)
    "Extract the bytevector corresponding to @var{hashcode}
(read-only)"
    (slice-copy/bytevector (hashcode:256->slice hashcode))))
