;; This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;; Copyright (C) 2023 GNUnet e.V.
;;
;; Scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; Scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(define-library (gnu gnunet utils records)
  (export define-record-type*)
  ;; keyword? cannot be used from (srfi srfi-88) because that sets
  ;; a reader option.
  (import (only (guile) define* lambda* keyword? error define-values pk syntax-error
		symbol->keyword)
	  (only (ice-9 match) match)
	  (only (rnrs base)
		begin define lambda define-syntax cons quasiquote quote unquote
		unquote-splicing apply reverse append null? eq? and not if
		string? values map assert car cdr cadr cddr let or pair?
		=> let* length list)
	  (only (rnrs control) when unless)
	  (only (rnrs syntax-case)
		syntax quasisyntax unsyntax unsyntax-splicing syntax-case
		syntax->datum identifier? generate-temporaries datum->syntax
		free-identifier=?)
	  (only (rnrs records syntactic) define-record-type)
	  (only (srfi srfi-1) assoc partition)
	  ;; in generated code
	  (only (rnrs base) =)
	  (only (gnu gnunet netstruct syntactic)
		define-analyser sizeof)
	  (only (gnu gnunet utils bv-slice)
		slice? slice-readable? slice-length
		slice-contents-equal? slice/read-only
		slice-copy/read-only))
  (begin
    (define unset (cons #false #false))

    (define* (process fields^ <type> type?
		      #:key
		      (constructor-keyword-arguments unset)
		      (constructor unset)
		      (constructor/copy unset)
		      (read-only-slice-wrapper #false)
		      (equality unset)
		      (analyse unset)
		      (copy unset)
		      (unwrap unset)
		      (network-structure unset))
      (define fields*
	(match (syntax->datum read-only-slice-wrapper)
	  (#true
	   (unless (null? fields^)
	     (error "fields may not be manually defined when #:read-only-slice-wrapper is #true"))
	   (when (eq? network-structure unset)
	     (error "when #:read-only-slice-wrapper is set, #:network-structure must be defined"))
	   (when (eq? unwrap unset)
	     (error "when #:read-only-slice-wrapper is set, #:unwrap must be defined"))
	   `((,#'slice
	      (#:getter . ,unwrap)
	      ;; Readability is checked by 'preprocess', to get a nice
	      ;; '&missing-capabilities' exception.
	      (#:predicate . ,#`(lambda (s)
				  (and (slice? s)
				       (= (slice-length s)
					  (sizeof #,network-structure '())))))
	      (#:preprocess . ,#'slice/read-only)
	      (#:equality . ,#'slice-contents-equal?))))
	  (#false fields^)))
      (when (and (not (eq? unwrap unset))
		 (eq? read-only-slice-wrapper unset))
	(error "#:unwrap can only be used in combination with #:read-only-slice-wrapper #true"))
      ;; s: unset, or syntax of the form '(identifier "docstring")',
      ;; or syntax of the form 'identifier'.
      ;;
      ;; Return types: (unset | identifier), syntax
      (define (maybe-identifier-maybe-with-docstring s)
	(if (eq? s unset)
	    (values unset #false)
	    (syntax-case s ()
	      ((id docstring)
	       (and (identifier? #'id) (string? (syntax->datum #'docstring)))
	       (values #'id #'docstring))
	      (id
	       (identifier? #'id)
	       (values #'id #false)))))
      (define-values (constructor** constructor-docstring)
	(maybe-identifier-maybe-with-docstring constructor))
      (define constructor*
	(if (eq? constructor** unset)
	    ;; define-record-type always requires a constructor
	    (car (generate-temporaries '(stuff)))
	    constructor**))
      (define-values (equality* equality-docstring)
	(maybe-identifier-maybe-with-docstring equality))
      (define-values (analyse* analyse-docstring)
	(maybe-identifier-maybe-with-docstring analyse))
      (define-values (copy* copy-docstring)
	;; The generated code for 'constructor/copy*' expects
	;; a 'copy' procedure to exist.
	(if (and (eq? copy unset) (not (eq? constructor/copy unset)))
	    (values (car (generate-temporaries '(copy))) #false)
	    (maybe-identifier-maybe-with-docstring copy)))
      (define-values (constructor/copy* constructor/copy-docstring)
	(maybe-identifier-maybe-with-docstring constructor/copy))
      (define (field-name field) ; -> identifier
	(car field))
      (define (field-verify field-name/different field)
	(if (field-set field #:predicate)
	    #`(assert (#,(field-ref field #:predicate) #,field-name/different))
	    #'#true)) ; exact value doesn't matter
      (define (field-compare field this that)
	(define g (field-ref field #:getter)) ; always defined
	#`(#,(field-ref field #:equality) ; sometimes undefined
	   (#,g #,this)
	   (#,g #,that)))
      (define (field->analyse-fragment field)
	;; TODO: #:network-structure-read, e.g. for when it's just a number?
	#`(#,(field-ref field #:analyse) ; sometimes undefined
	   #,(field-ref field #:network-structure-select))) ; sometimes undefined
      (define (field-clause field)
	#`(immutable #,(field-name field)
		     #,(field-ref field #:getter)))
      ;; TODO bail out if unrecognised field settings
      (define (field-copy field object)
	#`(#,(field-ref field #:copy) (#,(field-ref field #:getter) #,object)))

      ;; The same symbols as in (map field-name fields*), but as different
      ;; identifiers, to avoid field values from accidentally being used before they
      ;; have been preprocessed.  They are equal as symbols, such that
      ;; 'procedure-arguments' and the like produce something legible.
      (define field-names/different
	(map (lambda (f template-id)
	       (datum->syntax template-id (syntax->datum (field-name f))))
	     fields* (generate-temporaries fields*)))
      ;; Syntax.  The 'arguments' in the (define* (constructor . arguments) ...).
      ;; The idea is that default arguments can be passed with
      ;;   arguments = (foo #:key (bar 0) ...).
      (define constructor-keyword-arguments*
	(if (eq? constructor-keyword-arguments unset)
	    (map field-name fields*)
	    constructor-keyword-arguments))
      ;; TODO: check that constructor-keyword-arguments
      ;; contains all the field names.
      (define constructor-keyword-arguments*/different
	(let* ((names->different-alist
		(map cons (map field-name fields*) field-names/different))
	       (replacement
		(lambda (i)
		  (let ((matches
			 (partition (lambda (p)
				      (free-identifier=? (car p) i))
				    names->different-alist)))
		    (assert (= (length matches) 1))
		    (cdr (car matches))))))
	  (let loop ((s constructor-keyword-arguments*)
		     (bindings #'()))
	    (syntax-case s ()
	      ((keyword . rest)
	       (keyword? (syntax->datum #'keyword))
	       #`(keyword . #,(loop #'rest bindings)))
	      ((i . rest)
	       (identifier? #'i)
	       (let ((j (replacement #'i)))
		 #`(#,j . #,(loop #'rest #`(#,@bindings (i #,j))))))
	      (i ; (... . var)
	       (identifier? #'i)
	       (replacement #'i))
	      (((i value) . rest)
	       ;; 'value' can refer to previous arguments, so add some
	       ;; 'let' bindings to correct for the renaming.  (Untested;
	       ;; there are no users of this at time of writing.)
	       (let ((j (replacement #'i)))
		 #`((#,j (let #,bindings value))
		    . #,(loop #'rest #`(#,@bindings (i #,j))))))
	      (() s)
	      (_ (pk s)
		 (error "invalid keyword argument syntax in constructor"))))))
      (define (keywordify positional-arguments keyword-signature)
	(define (keywordify* positional-arguments keyword-signature positional?)
	  (define (something id rest)
	    (assert (identifier? id))
	    (assert (pair? positional-arguments))
	    #`(#,@(if positional?
		      #'()
		      #`(#,(datum->syntax #f (symbol->keyword (syntax->datum id)))))
	       #,(car positional-arguments)
	       . #,(keywordify* (cdr positional-arguments)
				rest
				positional?)))
	  (syntax-case keyword-signature ()
	    ((#:rest _) ; untested
	     (begin
	       (assert (= (length positional-arguments) 1))
	       #`(#,(car positional-arguments))))
	    ((#:allow-other-keys . r)
	     (keywordify* positional-arguments #'r positional?))
	    ((#:key . r)
	     (keywordify* positional-arguments #'r #false))
	    (((i default) . r) (something #'i #'r))
	    ((i . r)
	     (something #'i #'r))
	    (() #'((list)))))
	(keywordify* positional-arguments keyword-signature #true))
      (define (preprocess-arguments body)
	;; First, use field-names/different as constructor arguments.
	;; Otherwise, the preprocessors might accidentally use an
	;; un-preprocessed field.  Then, gradually
	;; re-introduce the field names, but with their preprocessed
	;; values.  Lastly, insert 'body'.
	(define (preprocess field-name/different field)
	  #`(#,(field-name field)
	     #,(if (field-set field #:preprocess)
		   (syntax-case (field-ref field #:preprocess) (=>)
		     ((=> expr)
		      #`(let ((#,(field-name field) #,field-name/different))
			  expr))
		     (proc #`(proc #,field-name/different)))
		   ;; nothing to preprocess, just unstash things.
		   field-name/different)))
	#`(let* #,(map preprocess field-names/different fields*)
	    #,body))
      #`(begin
	  (define-record-type (#,<type> #,constructor* #,type?)
	    (fields #,@(map field-clause fields*))
	    (protocol
	     (lambda (%make)
	       (lambda* #,constructor-keyword-arguments*/different
		 #,constructor-docstring
		 #,@(map field-verify field-names/different fields*)
		 #,(preprocess-arguments
		    #`(%make #,@(map field-name fields*))))))
	    (sealed #true)
	    (opaque #true))
	  #,@(if (eq? equality* unset)
		 #'()
		 #`((define (#,equality* this that)
		      #,equality-docstring
		      (and #,@(map (lambda (f) (field-compare f #'this #'that)) fields*)))))
	  #,@(if (eq? analyse* unset)
		 #'()
		 #`((define-analyser #,analyse* #,network-structure
		      #,analyse-docstring
		      (#,constructor*
		       #,@(map field->analyse-fragment fields*)))))
	  #,@(if (eq? copy* unset)
		 #'()
		 ;; When possible, avoid having to define a #:copy
		 ;; procedure and sort-of 'batch' allocations a bit.
		 (syntax-case read-only-slice-wrapper ()
		   (#true
		    #`((define (#,copy* slice)
			 (#,constructor*
			  (slice-copy/read-only
			   (#,(field-ref (car fields*) #:getter) slice))))))
		   ;; If not, just copy fields one-by-one.
		   (#false
		    #`((define (#,copy* object)
			 (apply #,constructor*
				#,@(keywordify (map (lambda (f) (field-copy f #'object))
						    fields*)
					       constructor-keyword-arguments*/different)))))))
	  #,@(if (eq? constructor/copy* unset)
		 #'()
		 #`((define* (#,constructor/copy* #,@constructor-keyword-arguments*)
		      #,constructor/copy-docstring
		      (#,copy* (apply #,constructor*
				      #,@(keywordify (map field-name fields*)
						     constructor-keyword-arguments*/different))))))))

    (define (field-ref field keyword)
      (match (assoc keyword (cdr field))
        ((key . value) value)
	(_ (pk 'field-ref field keyword)
	   (error "missing keyword in field"))))

    (define (field-set field keyword)
      (pair? (assoc keyword (cdr field))))

    (define (decompose-field-syntax stuff)
      (define (decompose-field-syntax-stuff* rest accumulated)
	(syntax-case rest ()
	  ;; Assuming no duplicates, the order of the keyword arguments
	  ;; doesn't matter, so no reversal needed here.
	  (() accumulated)
	  ((keyword value . rest*)
	   (keyword? (syntax->datum #'keyword))
	   (decompose-field-syntax-stuff*
	    #'rest*
	    `((,(syntax->datum #'keyword) . ,#'value) ,@accumulated)))))
      (syntax-case stuff ()
	((name . rest)
	 (cons #'name (decompose-field-syntax-stuff* #'rest '())))))

    (define (decompose-syntax s accumulated-fields accumulated-arguments k . k*)
      (syntax-case s ()
	;; order of keyword arguments doesn't matter, so no reversal there.
	;; (it is assumed there are no duplicates)
	(() (apply k (reverse accumulated-fields)
		   (append k* accumulated-arguments)))
	((#:field stuff . rest)
	 (apply decompose-syntax
		#'rest
		(cons (decompose-field-syntax #'stuff) accumulated-fields)
		accumulated-arguments
		k k*))
	((keyword value . rest) ; not #:field
	 (keyword? (syntax->datum #'keyword))
	 (apply decompose-syntax
		#'rest
		accumulated-fields
		`(,(syntax->datum #'keyword) ,#'value ,@accumulated-arguments)
		k k*))))

    (define-syntax define-record-type*
      (lambda (s)
	(syntax-case s ()
	  ((_ (<type> type?) . stuff)
	   (decompose-syntax #'stuff '() '() process #'<type> #'type?)))))))
