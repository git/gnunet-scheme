;#!r6rs
;; This file is part of scheme-GNUnet.
;; Copyright (C) 2021 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Brief: C-style enumeration that are platform-specific.
;; TODO: autoconfery

(define-module (gnu gnunet utils platform-enum)
  #:export (that-platform? define-platform-enumeration))

;; Define an enumeration, with values depending
;; on the platform.  If the value is not supported
;; on the target platform, set it to #f instead.

(define-syntax that-platform?
  (syntax-rules (or and not)
    ((_ #:hurd) (string-prefix? "gnu" (target-os)))
    ((_ #:linux) (string-prefix? "linux" (target-os)))
    ((_ (or a ...)) ((@ (guile) or) (that-platform? a) ...))
    ((_ (and a ...)) ((@ (guile) and) (that-platform? a) ...))
    ((_ (not a)) ((@ (guile) not) (that-platform? a)))
    ((_ #t) #t)
    ((_ #f) #f)
    ((_ #nil) #f)))

(define-syntax define-platform-enumeration
  (syntax-rules ()
    ((_ (name (platform value) ...) ...)
     (begin
       (define name
	 (cond ((that-platform? platform) value)
	       ...
	       (#t #f)))
       ...))))
