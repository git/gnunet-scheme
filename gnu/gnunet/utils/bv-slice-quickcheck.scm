;; This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;; Copyright © 2022 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Arbitraries for bytevector slices (incomplete).
;; TODO rename.
(define-library (gnu gnunet utils bv-slice-quickcheck)
  (export $sized-bytevector-slice/read-write
	  $sized-bytevector-slice/read-only)
  (import (only (rnrs base) define begin)
	  (only (quickcheck arbitrary)
		arbitrary arbitrary-xform $bytevector $record)
	  (only (quickcheck generator) choose-bytevector)
	  (only (gnu gnunet utils bv-slice)
		bv-slice/read-write bv-slice/read-only slice-copy/bytevector))
  (begin
    ;; TODO upstream
    (define ($sized-bytevector size)
      (arbitrary
       (gen (choose-bytevector size))
       (xform (arbitrary-xform $bytevector))))

    (define ($sized-bytevector-slice/read-write size)
      "Arbitrary fresh read-write bytevector slices of @var{size} octets."
      ($record bv-slice/read-write
	       (slice-copy/bytevector ($sized-bytevector size))))

    (define ($sized-bytevector-slice/read-only size)
      "Arbitrary read-only bytevector slices of @var{size} octets."
      ;; Currently fresh, but not guaranteed.
      ($record bv-slice/read-only
	       (slice-copy/bytevector ($sized-bytevector size))))))
