;#!r6rs
;;   This file is part of scheme-GNUnet, a partial Scheme port of GNUnet
;;   Copyright (C) 2001-2013, 2021 GNUnet e.V.
;;
;;   GNUnet is free software: you can redistribute it and/or modify it
;;   under the terms of the GNU Affero General Public License as published
;;   by the Free Software Foundation, either version 3 of the License,
;;   or (at your option) any later version.
;;
;;   GNUnet is distributed in the hope that it will be useful, but
;;   WITHOUT ANY WARRANTY; without even the implied warranty of
;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;   Affero General Public License for more details.
;;
;;   You should have received a copy of the GNU Affero General Public License
;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;   SPDX-License-Identifier: AGPL-3.0-or-later

;; From upstream:

;; @file include/gnunet_crypto_lib.h
;; @brief cryptographic primitives for GNUnet
;;
;; @author Christian Grothoff
;; @author Krista Bennett
;; @author Gerd Knorr <kraxel@bytesex.org>
;; @author Ioana Patrascu
;; @author Tzvetan Horozov
;; @author Jeffrey Burdges <burdges@gnunet.org>
;;
;; @defgroup crypto  Crypto library: cryptographic operations
;; Provides cryptographic primitives.
;;
;; @see [Documentation](https://gnunet.org/crypto-api)
;;
;; @defgroup hash  Crypto library: hash operations
;; Provides hashing and operations on hashes.
;;
;; @see [Documentation](https://gnunet.org/crypto-api)

;; Downstream (scheme-GNUnet)
;;
;; @brief
;;  Definition of cryptographic structures transmitted
;;  over the network.
;; @author Maxime Devos
(define-library (gnu gnunet crypto struct)
  (export /eddsa-signature /ecdsa-signature
	  /eddsa-public-key /ecdsa-public-key
	  /peer-identity
	  /ecdhe-public-key /ecdhe-private-key
	  /ecdsa-private-key /eddsa-private-key
	  /symmetric-session-key
	  /challenge-nonce-p
	  /ecc-signature-purpose)
  (import (only (gnu gnunet hashcode struct) /hashcode:256)
	  (only (gnu gnunet netstruct syntactic)
		define-type structure/packed)
	  (only (gnu gnunet netstruct procedural)
		u8vector u32/big)
	  (only (rnrs base) begin quote))
  (begin
    (define-type /eddsa-signature
      (structure/packed
       (synopsis "An ECC signature using EdDSA.")
       (documentation "See cr.yp.to/papers.html#ed25519")
       (properties '((c-type . GNUNET_CRYPTO_EddsaSignature)))
       (field (r (u8vector 64))
	      (synopsis "R value"))
       (field (s (u8vector 64))
	      (synopsis "S value"))))

    (define-type /ecdsa-signature
      (structure/packed
       (synopsis "An ECC signature using ECDSA.")
       (properties '((c-type . GNUNET_CRYPTO_EcdsaSignature)))
       (field (r (u8vector 64))
	      (synopsis "R value"))
       (field (s (u8vector 64))
	      (synopsis "S value"))))

    (define-type /eddsa-public-key
      (structure/packed
       (documentation "Public ECC key (always for curve Ed25519) encoded in
a format suitable for network transmission and EdDSA signatures.
Refer to section 5.1.3 of rfc8032, for a thorough explanation of how this
value maps to the x- and y-coordinates.")
       (properties '((c-type . GNUNET_CRYPTO_EddsaPublicKey)))
       (field (q (u8vector 32))
	      (documentation "Point Q consists of a y-value mod p (256 bits);
the x-value is always positive. The point is stored in Ed25519 standard
compact format."))))

    (define-type /ecdsa-public-key
      (structure/packed
       (documentation
	"Public ECC key (always for Curve25519) encoded in a format suitable
for network transmission and ECDSA signatures.")
       (properties '((c-type . GNUNET_CRYPTO_EcdsaPublicKey)))
       (field (q-y (u8vector 32))
	      (documentation
	       "Q consists of an x- and a y-value, each mod p (256 bits), given
here in affine coordinates and Ed25519 standard compact format."))))

    (define-type /peer-identity
      (structure/packed
       (synopsis
	"The identity of the host (wraps the signing key of the peer).")
       (properties '((c-type . GNUNET_PeerIdentity)))
       (field (public-key /eddsa-public-key))))

    (define-type /ecdhe-public-key
      (structure/packed
       (documentation
	"Public ECC key (always for Curve25519) encoded in a format suitable
for network transmission and encryption (ECDH),
See http://cr.yp.to/ecdh.html")
       (properties '((c-type . GNUNET_CRYPTO_EcdhePublicKey)))
       (field (q-y (u8vector 6))
	      (documentation
	       "Q consists of an x- and a y-value, each mod p (256 bits), given
here in affine coordinates and Ed25519 standard compact format."))))

    (define-type /ecdhe-private-key
      (structure/packed
       (synopsis "Private ECC key encoded for transmission")
       (properties '((c-type . GNUNET_CRYPTO_EcdhePrivateKey)))
       (documentation
	"To be used only for ECDH key exchange (ECDHE to be precise)")
       (field (d (u8vector 64))
	      (documentation
	       "d is a value mod n, where n has at most 256 bits"))))

    (define-type /ecdsa-private-key
      (structure/packed
       (synopsis "Private ECC key encoded for transmission")
       (documentation
	"To be used only for ECDSA signatures.")
       (properties '((c-type . GNUNET_CRYPTO_EcdsaPrivatecKey)))
       (field (d (u8vector 64))
	      (documentation
	       "d is a value mod n, where n has at most 256 bits"))))

    (define-type /eddsa-private-key
      (structure/packed
       (synopsis "Private ECC key encoded for transmission")
       (documentation "To be used only for EdDSA signatures.")
       (properties '((c-type . GNUNET_CRYPTO_EddsaPrivateKey)))
       (field (d (u8vector 64))
	      (documentation
	       "d is a value mod n, where n has at most 256 bits."))))

    (define-type /symmetric-session-key
      (structure/packed
       (synopsis "Type for session keys")
       (properties '((c-type . GNUNET_CRYPTO_SymmetricSessionKey)))
       (field (aes-key (u8vector 64))
	      (synopsis "Actual key for AES"))
       (field (twofish-key (u8vector 64))
	      (synopsis "Actual key for TwoFish"))))

    (define-type /challenge-nonce-p
      (structure/packed
       (synopsis "Type of a nonce used for challenges")
       ;; TODO: https://bugs.gnunet.org/view.php?id=7173
       (properties '((c-type . ChallengeNonceP)))
       (field (value /hashcode:256)
	      (synopsis
	       "The value of the nonce.  Note that this is NOT a hash."))))

    (define-type /ecc-signature-purpose
      (structure/packed
       (synopsis "Header indicating what an ECC signature signs")
       (properties '((c-type . GNUNET_CRYPTO_EccSignaturePurpose)))
       (field (size u32/big)
	      (synopsis "The number of bytes the signature signs"))
       (field (purpose u32/big)
	      (synopsis "What does this signature vouch for?"))))))

