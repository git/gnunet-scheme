;#!r6rs
;;   This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;;   Copyright (C) 2001--2013, 2020 GNUnet e.V.
;;
;;   GNUnet is free software: you can redistribute it and/or modify it
;;   under the terms of the GNU Affero General Public License as published
;;   by the Free Software Foundation, either version 3 of the License,
;;   or (at your option) any later version.
;;
;;   GNUnet is distributed in the hope that it will be useful, but
;;   WITHOUT ANY WARRANTY; without even the implied warranty of
;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;   Affero General Public License for more details.
;;
;;   You should have received a copy of the GNU Affero General Public License
;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;   SPDX-License-Identifier: AGPL-3.0-or-later
;;
;;   As a special exception to the GNU Affero General Public License,
;;   the file may be relicensed under any license used for
;;   most source code of GNUnet 0.13.1, or later versions, as published by
;;   GNUnet e.V.

;; Extracted from src/util/crypto_hash.c by Christian Grothoff

(library (gnu gnunet hashcode-ascii (1 1))
  (export hashcode->ascii ascii->hashcode
          #; hashcode-ascii-length)
  (import (gnu gnunet hashcode)
          (gnu gnunet data-string)
	  (rnrs base))

  ;; In upstream, this is 104 for the \0 terminator
  #;(define hashcode-ascii-length 103)

  (define (hashcode->ascii hashcode)
    "Convert a <hashcode:512> to ASCII encoding.  The ASCII encoding is rather
GNUnet specific.  It was chosen such that it only uses characters
in [0-9A-V], can be produced without complex arithmetics and uses a
small number of characters.  The GNUnet encoding uses 103
characters.

@var{block} the hash code"
    (data->string (hashcode->bv hashcode)))

  (define (ascii->hashcode:512 ascii)
    "Convert ASCII encoding back to hash code.

@var{ascii} the encoding
Return @lisp{#f} in case of an encoding error."
    (let ((bv (guard (c ((bogus-crockford-base32hex? c) #false))
		     (string->data ascii hashcode:512-u8-length))))
      (and bv (bv->hashcode:512 bv)))))
