;#!r6rs
;; -*- scheme -*- Constants for network protocols
;; This file is part of scheme-GNUnet.
;; Copyright (C) 2001--2021 GNUnet e.V.
;; SPDX-License-Identifier: AGPL-3.0-or-later
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

(let-syntax ((the-library-form
	      (lambda (s)
		(let ()
		  (define (include/sexp name)
		    (define file
		      (search-path %load-path
				   (string-append "gnu/gnunet/message/" name)))
		    (call-with-input-file file
		      (lambda (port)
			(let loop ()
			  (let ((obj (read port)))
			    (if (eof-object? obj)
				'()
				(cons obj (loop))))))))
		  ;; Do this instead of (include "protocols.scmgen") to placate
		  ;; "make distcheck".
		  (include-from-path "gnu/gnunet/message/protocols.scmgen")
		  (datum->syntax s result)))))
  the-library-form)
