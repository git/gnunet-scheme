;; -*- scheme -*- Copyright © 2001--2021 GNUnet e.V.
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; * IDENTITY messsage types
(value
 (symbol msg:identity:start!)
 (index 624)
 (documentation "First message send from identity client to service
(to subscribe to updates)."))
(value
 (symbol msg:identity:result-code)
 (index 625)
 (documentation "Generic response from identity service with success
and/or error message."))
(value
 (symbol msg:identity:update)
 (index 626)
 (documentation "Update about identity status from service to clients."))
(value
 (symbol msg:identity:get-default)
 (index 627)
 (documentation "Client requests to know default identity for a subsystem."))
(value
 (symbol msg:identity:set-default!)
 (index 628)
 (documentation "Client sets default identity; or service informs
about default identity."))
(value
 (symbol msg:identity:create!)
 (index 629)
 (documentation "Create new identity (client->service)."))
(value
 (symbol msg:identity:rename!)
 (index 630)
 (documentation "Rename existing identity (client->service)"))
(value
 (symbol msg:identity:delete!)
 (index 631)
 (documentation "Delete identity (client->service)."))
(value
 (symbol msg:identity:lookup)
 (index 632)
 (documentation "First message send from identity client to service to
lookup a single ego.  The service will respond with a
msg:identity:update message if the ego exists, or a
msg:identity:result-code if not."))
(value
 (symbol msg:identity:lookup-by-suffix)
 (index 633)
 (documentation "First message send from identity client to service
to lookup a single ego matching the given suffix (longest match).
The service will respond with a msg:identity:update message if the
ego exists, or a msg:identity:result-code if not."))
