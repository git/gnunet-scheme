;#!r6rs
;; This file is part of GNUnet.
;; Copyright (C) 2006-2021 GNUnet e.V.
;;
;; GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Brief: many network structures, that would otherwise result in very
;; small source files if each was put in their own module.
(define-library (gnu gnunet util struct)
  (export /uuid /time-absolute
	  /:message-header /:operation-result /async-scope-id)
  (import (only (gnu gnunet netstruct syntactic)
		define-type structure/packed)
	  (only (gnu gnunet netstruct procedural)
		u8vector u16/big u32/big u64/big)
	  (only (rnrs base) begin quote))
  (begin
    ;; Absolute time (in GNUnet), in microseconds
    (define-type /time-absolute u64/big)

    (define-type /uuid
      (structure/packed
       (synopsis "A UUID, a 128 bit random value")
       (properties '((c-type . GNUNET_Uuid)))
       (field (value/u8 (u8vector 16))
	      (synopsis "128 random bits")
	      (documentation
	       "This is represented as an array of uint32 in GNUnet"))))

    ;; If this definition is ever changed (breaking compatibility with
    ;; C GNUnet), make sure to change (gnu gnunet utils tokeniser)
    ;; appropriately as well -- the field types and positions are
    ;; hardcoded there.
    (define-type /:message-header
      (structure/packed
       (synopsis "Header for all communications")
       (properties '((c-type . GNUNET_MessageHeader)))
       (field (size u16/big)
	      (documentation
	       "The length of the struct (in bytes, including the length
field itself), in big-endian format."))
       (field (type u16/big)
	      (synopsis "The type of the message")
	      (documentation
	       "The type of the message (GNUNET_MESSAGE_TYPE_XXXX in the C
implementation and msg:XXX:YYY:... in the Scheme implementation),
in big-endian format."))))

    (define-type /:operation-result
      (structure/packed
       (synopsis "Answer from service to client about last operation")
       (documentation "Possibly followed by data")
       (properties '((c-type . GNUNET_OperationResultMessage)))
       (field (header /:message-header))
       (field (reserved u32/big))
       (field (operation-id u64/big)
	      (synopsis "Operation ID"))
       (field (result-code u64/big)
	      (synopsis "Status code for the operation"))))

    (define-type /async-scope-id
      (structure/packed
       (synopsis "Identifier for an asynchronous execution context")
       (documentation
	"This is represented as an array of uint32_t in GNUnet.")
       (properties '((c-type . GNUNET_AsyncScopeId)))
       (field (bits/u8 (u8vector 16)))))))
