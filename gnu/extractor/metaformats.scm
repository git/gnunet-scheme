;;   This file is part of scheme-extractor.
;;   scheme-extractor is a partial Scheme port of libextractor.
;;   A previous iteration of this file is part of libextractor.
;;   Copyright (C) 2002-2017 Vidyut Samanta and Christian Grothoff
;;   Copyright (C) 2020 GNUnet e.V.
;;
;;   libextractor is free software; you can redistribute it and/or modify
;;   it under the terms of the GNU General Public License as published
;;   by the Free Software Foundation; either version 3, or (at your
;;   option) any later version.
;;
;;   libextractor is distributed in the hope that it will be useful, but
;;   WITHOUT ANY WARRANTY; without even the implied warranty of
;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;   General Public License for more details.
;;
;;   You should have received a copy of the GNU General Public License
;;   along with libextractor; see the file COPYING.  If not, write to the
;;   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;;   Boston, MA 02110-1301, USA.

;; SPDX-License-Identifier: GPL-3.0-or-later
;; Upstream source: src/include/extractor.h

(library (gnu extractor metaformats)
  (export meta-format?
	  meta-format->integer
	  integer->meta-format
	  METAFORMAT_UNKNOWN
	  METAFORMAT_UTF8
	  METAFORMAT_BINARY
	  METAFORMAT_C_STRING)
  (import (gnu extractor enum))

  (define-wrapped-enum (<meta-format> meta-format? integer->meta-format
				      meta-format->integer)
    (#:max 4294967295)
    (#:known
     ;; Format is unknown.
     (METAFORMAT_UNKNOWN 0)
     ;; 0-terminated, UTF-8 encoded string.  "data_len"
     ;; is strlen(data)+1.
     (METAFORMAT_UTF8 1)
     ;; Some kind of binary format, see given Mime type.
     (METAFORMAT_BINARY 2)
     ;; 0-terminated string.  The specific encoding is unknown.
     ;; "data_len" is strlen (data)+1.
     (METAFORMAT_C_STRING 3))))
