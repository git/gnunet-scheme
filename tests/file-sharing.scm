;; This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;; Copyright © 2022, 2023 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(define-module (test-file-sharing))
(import (gnu gnunet fs client)
	(srfi srfi-64)
	(gnu gnunet crypto struct)
	(gnu gnunet fs client)
	(gnu gnunet fs network)
	(gnu gnunet fs quickcheck)
	(gnu gnunet fs uri)
	(gnu gnunet hashcode)
	(gnu gnunet utils bv-slice-quickcheck)
	(gnu gnunet netstruct syntactic)
	(quickcheck arbitrary)
	(ice-9 match)
	(tests utils))

(test-begin "file-sharing")

(define (normalise list)
  (pk list)
  (map (match-lambda
	((? content-hash-key? k)
	 (make-content-hash-key
	  (copy-hashcode:512 (content-hash-key-key k))
	  (copy-hashcode:512 (content-hash-key-query k))))
	(foo foo))
       list))

(define (construct-request-loc-signature* x y z w)
  (construct-request-loc-signature x y z #:purpose w))

(define (construct-response-loc-signature* x y z w)
  (construct-response-loc-signature x y z #:purpose w))

;; Network messages
(define $file-length $natural) ; TODO bounds
(define $purpose $natural) ; TODO bounds
(define $expiration-time $natural) ; TODO bounds
(define $peer-identity
  ($sized-bytevector-slice/read-write (sizeof /peer-identity '())))
(define $signature
  ($sized-bytevector-slice/read-write (sizeof /eddsa-signature '())))

(test-roundtrip "analyse + construct round-trips (request-loc-signature)"
		analyse-request-loc-signature construct-request-loc-signature*
		normalise
		(file-length $file-length)
		(content-hash-key $content-hash-key)
		(expiration-time $expiration-time)
		(purpose $purpose))

(test-roundtrip "analyse + construct round-trips (response-loc-signature)"
		analyse-response-loc-signature construct-response-loc-signature*
		normalise
		(expiration-time $expiration-time)
		(signature $signature)
		(peer-identity $peer-identity)
		(purpose $purpose))


;; Standard service tests
(test-assert "close, not connected --> all fibers stop, no callbacks called"
  (close-not-connected-no-callbacks "fs" (pk connect) disconnect!))
(test-assert "garbage collectable"
  (garbage-collectable "fs" connect))
(test-assert "notify disconnected after end-of-file, after 'connected'"
  (disconnect-after-eof-after-connected "fs" connect))
(test-assert "reconnects"
  (reconnects "fs" connect))
(test-end "file-sharing")
