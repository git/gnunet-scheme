;; This file is part of scheme-GNUnet.
;; Copyright (C) 2021 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(import (srfi srfi-26)
	(gnu gnunet netstruct procedural)
	(gnu gnunet utils bv-slice)
	(rnrs bytevectors)
	(prefix (gnu gnunet netstruct syntactic) s@))

(test-begin "netstruct procedural")

(define struct/a
  (make-netstructure
   (vector (make-field 'field u8))))

(define struct/b
  (make-netstructure
   (vector (make-field 'field u8)
	   (make-field 'other-field u8))))

(test-eqv "first field, offset (struct)"
  0
  (offsetof struct/a '(field)))

(test-equal "writing to a u16, big endian"
  (cons #xde #xad)
  (let ((s (make-slice/read-write 2)))
    (set%! u16/big '() s #xdead)
    (cons (slice-u8-ref s 0)
	  (slice-u8-ref s 1))))

(test-eqv "writing to (smaller) field"
  #xbe
  (let ((s (bv-slice/read-write (u8-list->bytevector '(#xbe #xef)))))
    (set%! struct/b '(field) s #xbe)
    (slice-u8-ref s 0)))

(test-eqv "writing to (smaller) field, at an offset"
  #xbe
  (let ((s (bv-slice/read-write (u8-list->bytevector '(#xbe #xef)))))
    (set%! struct/b '(other-field) s #xbe)
    (slice-u8-ref s 1)))

(test-eqv "reading from (smaller) field"
  #xbe
  (let ((s (bv-slice/read-write #u8(#xbe #xef))))
    (read% struct/b '(field) s)))

(test-eqv "reading from (smaller) field, at an offset"
  #xef
  (let ((s (bv-slice/read-write #u8(#xbe #xef))))
    (read% struct/b '(other-field) s)))

(test-eqv "writing to a u8"
  #xde
  (let ((s (make-slice/read-write 1)))
    (set%! u8 '() s #xde)
    (slice-u8-ref s 0)))

;; While the individual fields can be written / read,
;; the parts cannot!
(test-error "writing a structure -> error"
  &unwritable
  (set%! struct/b '() (make-slice/read-write 2) 'anything))

(test-error "reading a structure -> error"
  &unreadable
  (read% struct/b '() (make-slice/read-write 2)))

(test-eq "recursive part"
  u8
  (part (make-netstructure
	 (vector (make-field 'recurse struct/a)))
	'(recurse field)))

(test-end "netstruct procedural")
(test-begin "nestruct syntactic")

(test-eqv "total size (syntactic, constant, one field)"
  1 (eval '(s@sizeof struct/a '())
	  (current-module)))

(test-eqv "field size (syntactic, constant, one field)"
  1 (eval '(s@sizeof struct/a '(field))
	  (current-module)))

(test-end "nestruct syntactic")
