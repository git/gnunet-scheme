;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021, 2022 GNUnet e.V.
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;;  SPDX-License-Identifier: GPL-3.0-or-later

(use-modules (gnu packages guile)
	     (gnu packages guile-xyz)
	     (gnu packages autotools)
	     (gnu packages gettext)
	     (gnu packages gnupg)
	     (gnu packages pkg-config)
	     (gnu packages xorg)
	     (gnu packages text-editors)
	     (guix packages)
	     (guix utils)
	     (guix gexp)
	     (guix git)
	     (guix git-download)
	     (guix download)
	     (guix build-system gnu)
	     ((guix licenses) #:prefix license:))

(define %source-dir (dirname (current-filename)))

(define-public scheme-gnunet
  (package
   (name "scheme-gnunet")
   (version "0.3")
   (source (local-file %source-dir
		       #:recursive? #t
		       #:select? (git-predicate %source-dir)))
   (build-system gnu-build-system)
   (propagated-inputs
    (list guile-zlib guile-bytestructures guile-fibers-1.1 guile-gcrypt
	  guile-json-4 guile-pfds))
   (native-inputs
    (list guile-3.0-latest guile-gcrypt guile-fibers-1.1 guile-json-4 guile-pfds
	  automake
	  ;; Only used for testing.
	  guile-quickcheck
          ;; Gettext brings 'AC_LIB_LINKFLAGS_FROM_LIBS'.
	  gettext-minimal
	  pkg-config
	  autoconf-wrapper
	  texmacs
	  xvfb-run))
   (inputs (list guile-3.0-latest))
   (synopsis "Guile implementation of GNUnet client libraries")
   (license license:agpl3+)
   ;; Description for new version
   (description "This package provides Guile modules for connecting to various
GNUnet services. It also has infrastructure for writing new GNUnet services and
connecting to them and can be used from multi-threaded environments.  It is not
to be confused with @code{guile-gnunet} -- @code{guile-gnunet} supports a different
set of services.

The following services are supported:

@itemize
@item NSE (network size estimation)
@item DHT (distributed hash table)
@item CADET (secure end-to-end communication between arbitrary peers)
@end itemize")
   (home-page #f)))
scheme-gnunet
